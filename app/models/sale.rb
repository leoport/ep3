class Sale < ApplicationRecord
  belongs_to :customer
  belongs_to :user
  belongs_to :discount
  has_many :product_quantities
end
