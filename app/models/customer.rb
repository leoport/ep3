class Customer < ApplicationRecord
  enum status: [:active, :inactive]
  has_one :address
  belongs_to :user
end
